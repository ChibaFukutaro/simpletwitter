package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns = { "/setting", "/edit" })

public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		System.out.println("ログインチェック");

		HttpServletResponse res = (HttpServletResponse)response;
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		// セッション領域にユーザー情報が存在しない場合NULLを返す
		if (session.getAttribute("loginUser") != null) {
			// セッション領域のユーザー情報がNULLでなければ、通常どおりの遷移
			chain.doFilter(request, response);
		} else {
			// セッション領域のユーザー情報がNullならば、ログイン画面へ飛ばす
			List<String> errorMessages = new ArrayList<String>();
			session.setAttribute("errorMessages", errorMessages);
			errorMessages.add("ログインしてください");
			res.sendRedirect("./login");
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}